import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginToSalesforceTest {

    @Test
    public void shouldCorrectLoginToSalesforce() {
        String USERNAME = "";
        String PASSWORD = "";
        String userFirstName = "User";
        String userLastName = "User";

        //Setup
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 30L, 500L);

        //Open login page
        driver.get("https://test.salesforce.com");

        //Input username
        driver.findElement(By.cssSelector("input#username")).sendKeys(new CharSequence[]{USERNAME});

        //Input password
        driver.findElement(By.cssSelector("input#password")).sendKeys(new CharSequence[]{PASSWORD});

        //Click 'Login'
        driver.findElement(By.cssSelector("input#Login")).click();

        //Get actual user name
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span[class='uiImage'][data-aura-class='uiImage']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("h1.profile-card-name>a")));
        String loggedUser = driver.findElement(By.cssSelector("h1.profile-card-name a")).getText();

        //Assertions
        Assertions.assertEquals(userFirstName + " " + userLastName, loggedUser, "Wrong user name!");

        //Teardown
        driver.quit();
    }
}
